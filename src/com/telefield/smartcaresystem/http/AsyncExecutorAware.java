package com.telefield.smartcaresystem.http;

public interface AsyncExecutorAware<T> {

	public void setAsyncExecutor(AsyncExecutor<T> asyncExecutor);

}