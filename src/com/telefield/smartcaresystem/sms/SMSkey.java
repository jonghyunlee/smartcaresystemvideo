package com.telefield.smartcaresystem.sms;

public interface SMSkey {
	/**
	 * Hot Line 전화번호 설정
	 */
	final String SMS_HOT_LINE_NUM = "UCF:0";
	/**
	 * 역점검 요청
	 */
	final String SMS_RESULT_CHECK = "UCF:3";
	/**
	 * 원격 개통
	 */
	final String SMS_REMOTE_OPEN = "TF_01";
	/**
	 * 시스템 모드 설정
	 * 
	 */
	final String SMS_SYSTEM_MODE_SETTING = "TF_02";
	/**
	 * 스마트폰 업데이트
	 */
	final String SMS_GATEWAY_FIRMWARE_UPDATE = "TF_03";
	/**
	 * 본체 업데이트
	 */
	final String SMS_ZIGBEE_FIRMWARE_UPDATE = "TF_04";
	/**
	 * 게이트웨이 상태
	 */
	final String SMS_GATEWAY_INFORMATION = "TF_05";
	/**
	 * 센서 상태
	 */
	final String SMS_SENSOR_INFORMATION = "TF_06";
	/**
	 * 재실 상태 보고
	 */
	final String SMS_IN_MODE_SETTING = "TF_07";
	/**
	 * 전원 복구 보고
	 */
	final String SMS_POWER_RECOVERY = "TF_08";
	/**
	 * 활동 감지 보고
	 */
	final String SMS_ACTIVITY = "TF_09";
	/**
	 * 데이터 미수신(원격 개통)
	 */
	final String SMS_DATA_NOT_RECEIVE = "TF_10";
	/**
	 * 타지역 원격 개통
	 */
	final String SMS_NOT_MATCH_AREA = "TF_11";
	
	static final int SMSFormatIndex = 0;
	static final int SMSCenterIndex = 1;
	static final int SMSNineoneoneIndex = 2;
	static final int SMSCareIndex_01 = 3;
	static final int SMSCareIndex_02 = 4;
	static final int SMSHelperIndex = 5;

	static final int SMSSystemModeIndex = 1;
	static final int SMSSystemModeElder = 1;
	static final int SMSSystemModeDisabled = 2;
	static final int SMSSystemModeMedicalIT = 3;
	static final int SMSSystemModeOneReinstalling = 4;
	
	static final int ACTIVITY_SENSOR_INDEX = 20;
	static final int FIRE_SENSOR_INDEX = 29;
	static final int GAS_SENSOR_INDEX = 39;
	static final int DOOR_SENSOR_INDEX = 10;
	static final int EMERGENCY_SENSOR_INDEX = 60;
}
