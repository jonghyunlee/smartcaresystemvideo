package com.telefield.smartcaresystem.sms;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;

import com.example.smartcaresystemvideo.util.HandleException;


public class SMSSender {
	Context context;
	final static String TAG = "SMSSender"; 
	
	public SMSSender(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	public void sendSMS(String phoneNumber, String returnMessage){
		PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0,new Intent("SMS_SENT_ACTION"), 0);
		PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0,new Intent("SMS_DELIVERED_ACTION"), 0);
		
		Log.d(TAG, "SMSSender sendSMS returnMessage.length : " + returnMessage.length() + "\n---------smsMessage---------\n" + returnMessage+"\n---------------------------");
		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					// 전송 성공
					Log.d(TAG, "SMSSender 전송 완료");
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					// 전송 실패
					Log.d(TAG, "SMSSender 전송 실패");
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					// 서비스 지역 아님
					Log.d(TAG, "SMSSender 서비스 지역이 아닙니다");
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					// 무선 꺼짐
					Log.d(TAG, "SMSSender 무선(Radio)가 꺼져있습니다");
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					// PDU 실패
					Log.d(TAG, "SMSSender PDU Null");
					break;
				}
			}
		}, new IntentFilter("SMS_SENT_ACTION"));

		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					// 도착 완료
					Log.d(TAG, "SMSSender SMS 도착 완료");
					break;
				case Activity.RESULT_CANCELED:
					// 도착 안됨
					Log.d(TAG, "SMSSender SMS 도착 실패");
					break;
				}
			}
		}, new IntentFilter("SMS_DELIVERED_ACTION"));

		SmsManager mSmsManager = SmsManager.getDefault();
//		mSmsManager.sendTextMessage(phoneNumber, null, returnMessage, sentIntent,
//				deliveredIntent);SMS(저용량 메시지용)
		ArrayList<String> parseMessage = mSmsManager.divideMessage(returnMessage);
		
		try{
			File file = new File("//storage/emulated/0/DCIM/Camera/1404574463840.jpg");
			FileInputStream in = new FileInputStream(file);
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			   
			byte[] by = new byte[(int)file.length()];
			int len = 0;
			 
			while( (len=in.read(by)) != -1)
			bout.write(by, 0, len);
			// 최종적인  byte[]로....   
			byte[] imgbuf = bout.toByteArray();
			bout.close();
			in.close();
			Log.d(TAG, "file size : " + file.length());
			Log.d(TAG, "imgbuf size : " + imgbuf.length);
			mSmsManager.sendDataMessage(phoneNumber, null, (short) 0, imgbuf, null, null);	
		}catch(Exception e){
			HandleException.writeException(e);
		}
		//		mSmsManager.sendMultipartTextMessage(phoneNumber, null, parseMessage, null, null);//MMS(대용량 메시지용)
	}
	
	public void SendMMS(/*ArrayList<String> urlString*/) {
		boolean exceptionCheck = false;

		String PATH = "//storage/emulated/0/DCIM/Camera/1404574463840.jpg";
//		Intent sendIntent = new Intent();
//
//		// Selection count
//		sendIntent.setAction(Intent.ACTION_SEND);
//		exceptionCheck = true;
//
//		if (!exceptionCheck) {
//			
//			sendIntent.addCategory("android.intent.category.DEFAULT");
//
//			ArrayList<Uri> uris = new ArrayList<Uri>();
//
//			File fileIn = new File("file://" + PATH);
//			Uri u = Uri.fromFile(fileIn);
//			uris.add(u);
//			
//			sendIntent.setType("image/jpeg");
//			Log.d(TAG, "filePATH : " + PATH);
//			sendIntent.putExtra(Intent.EXTRA_STREAM,
//					Uri.parse("file://" + PATH));
//
//		}
//
//		try {
//			Log.d(TAG, "send smsmsmsms");
//			context.startActivity(sendIntent);
//
//		} catch (Exception e) {
//			HandleException.writeException(e);
//		}MMEncoder
		Uri uri = Uri.parse("mmsto:");
		File file = new File("//storage/emulated/0/DCIM/Camera/1404574463840.jpg");
		Intent it = new Intent(Intent.ACTION_SEND, uri); 
		it.putExtra("address", "01051498848");
		it.putExtra("sms_body", "some text");   
		it.putExtra("exit_on_sent", true);
		it.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));   
		it.setType("image/*");  
//		it.setComponent(new ComponentName("com.sec.mms", "com.sec.mms.Mms"));
		context.startActivity(it);
//		Intent it = new Intent(Intent.ACTION_SENDTO, uri);   
//		it.putExtra("sms_body", "The SMS text");   
//		context.startActivity(it); 
     } 
	
	
}
