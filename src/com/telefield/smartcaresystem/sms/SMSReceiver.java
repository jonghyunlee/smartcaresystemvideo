package com.telefield.smartcaresystem.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.smartcaresystemvideo.util.HandleException;

public class SMSReceiver extends BroadcastReceiver{

	final static String TAG = "SMSReceiver"; 
	public SMSReceiver() {
		Log.d(TAG, "start SMSReceiver");
	}
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		abortBroadcast();
		final Bundle bundle = intent.getExtras();
		  
	        try {
	            if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
	            	SmsMessage currentMessage = null;
		            if (bundle != null) {
		                final Object[] pdusObj = (Object[]) bundle.get("pdus");
		                Log.d(TAG, "SMSReceiver pdusObj.length : " + pdusObj.length);
		                for (int i = 0; i < pdusObj.length; i++) {
	//	                	LogMgr.d(LogMgr.TAG, "SMSReceiver i : " + i);
		                    currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[0]); 
		                } // end for loop
		            } // bundle is null
		            String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    
                    String message = currentMessage.getDisplayMessageBody();
 
                    Log.d(TAG, "SMSReceiver senderNum: "+ phoneNumber + "; message: " + message);

                   /* 
                    int duration = Toast.LENGTH_SHORT;
                    Toast.makeText(context, "sms receive", duration).show();
            		Toast.makeText(context, "senderNum: "+ phoneNumber + ", message: " + message, duration).show();
                    */
//            		SMSHandler smsHandler = new SMSHandler();
//            		smsHandler.handleSMS(phoneNumber, message);
	            }
	        } catch (Exception e) {
	            HandleException.writeException(e);
	             
	        }
	}
}
