package com.example.smartcaresystemvideo;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.smartcaresystemvideo.util.Constant;

public class MenuListActivity extends Activity{
	Context context;
	ListView menuListView;
	MenuListAdapter listAdapter;
	HandleAudio recordAudio;
	String selectMode;
	boolean isAlert;
	String filePath;
	AlertDialog alertDialog;
	String recordList[];
	File file;
	File[] files;
	int audioPosition;
	String TAG = "MenuListActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menulistview);
		
		context = this;
		Intent intent = getIntent();
		
		selectMode = intent.getStringExtra(Constant.INTENT_SELECTMODE);
		isAlert = intent.getBooleanExtra(Constant.INTENT_ISALERT, false);
		filePath = intent.getStringExtra(Constant.INTENT_FILEPATH);
		
		Log.d(TAG, "MenuListActivity onCreate selectMode : " + selectMode);
//		selectMode = Constant.INTENT_SELECTVIDEO;
		
		menuListView = (ListView)findViewById(R.id.selectMenuList);
		
		
		AlertDialog.Builder alertDialogbuilder = new AlertDialog.Builder(this);
		alertDialogbuilder.setCancelable(true);
		if(isAlert){
			switch (selectMode) {
			case Constant.INTENT_SELECTVIDEO:
				alertDialogbuilder.setMessage("촬영한 동영상 메시지를 전송 하시겠습니까?");		
				break;
							
			case Constant.INTENT_SELECTAUDIO:
				alertDialogbuilder.setMessage("녹음한 음성 메시지를 전송 하시겠습니까?");	
				break;

			default:
				break;
			}
			alertDialogbuilder.setPositiveButton("전송", posotiveListener);
			alertDialogbuilder.setNegativeButton("취소", negativeListener);
		
			alertDialog = alertDialogbuilder.create();
			alertDialog.show();
		}
		
		menuListView.setOnItemClickListener(onItemClickListener);
		
		
	}
	@Override
	protected void onResume() {
		FilenameFilter fileFilter;
		int count = 0;
		switch (selectMode) {
		case Constant.INTENT_SELECTVIDEO:
			fileFilter = new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String filename) {
					
					return filename.endsWith("mp4");
				}
			};
			
			file = new File(Constant.VEDIO_RECORD_PATH);
			files = file.listFiles(fileFilter);
			
			recordList = new String[files.length];
			count = 0;
			for(int i = files.length - 1 ; i >= 0 ; i--){
				recordList[count] = files[i].getName();
				count++;
			}
			
			break;		
		case Constant.INTENT_SELECTAUDIO:
			fileFilter = new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String filename) {
					
					return filename.endsWith("mp4");
				}
			};
			
			file = new File(Constant.AUDIO_RECORD_PATH);
			files = file.listFiles(fileFilter);
			
			recordList = new String[files.length];
			count = 0;
			for(int i = files.length - 1 ; i >= 0 ; i--){
				recordList[count] = files[i].getName();
				count++;
			}
			break;
			
		case Constant.INTENT_SELECTLOG:
			
			break;

		default:
			break;
		}
		
		listAdapter = new MenuListAdapter(selectMode, context, recordList, handler);
		menuListView.setAdapter(listAdapter);
		super.onResume();
	};
	DialogInterface.OnClickListener posotiveListener = new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			File file = new File(filePath);
			if(file.exists()){
				switch (selectMode) {
				case Constant.INTENT_SELECTVIDEO:
					new ServerSend(ServerSend.VIDEO_MODE, filePath, context);
					new ServerSend.SendThread().execute();
					break;
								
				case Constant.INTENT_SELECTAUDIO:
					new ServerSend(ServerSend.AUDIO_MODE, filePath, context);
					new ServerSend.SendThread().execute();
					break;

				default:
					break;
				}
				
			}
		}
	};
	
	DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			
		}
	};
	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Log.e(TAG, "onItemClick : " + position);
			FilenameFilter fileFilter = null;
			Intent intent;
			switch(selectMode) {
			case Constant.INTENT_SELECTVIDEO:
				Log.e(TAG, "onItemClick : " + "INTENT_SELECTVIDEO");
				fileFilter = new FilenameFilter() {
					
					@Override
					public boolean accept(File dir, String filename) {
						
						return filename.endsWith("mp4");
					}
				};
				
				file = new File(Constant.VEDIO_RECORD_PATH);
				files = file.listFiles(fileFilter);
				
				intent = new Intent(context, VideoPlayerActivity.class);
				intent.putExtra(Constant.INTENT_PLAY_VIDEO_PATH, files[files.length - position -1].getPath());
				startActivity(intent);
				break;
				
			case Constant.INTENT_SELECTAUDIO:
				Log.e(TAG, "onItemClick : " + "INTENT_SELECTAUDIO");
				fileFilter = new FilenameFilter() {
					
					@Override
					public boolean accept(File dir, String filename) {
						
						return filename.endsWith("mp4");
					}
				};
				
				Toast.makeText(context, "음성 파일을 재생합니다.", Toast.LENGTH_SHORT).show();
				file = new File(Constant.AUDIO_RECORD_PATH);
				files = file.listFiles(fileFilter);

				recordAudio = new HandleAudio();
				filePath = files[files.length - position -1].getPath();
				recordAudio.setFilePath(filePath);
				recordAudio.setMode(HandleAudio.PLAY_MODE);
				recordAudio.show(((Activity) context).getFragmentManager(), "Dialog");
				
				break;
				
			case Constant.INTENT_SELECTLOG:
				
				break;
				
			}
		}
	};
	
	Handler handler = new Handler(){
		public void handleMessage(Message msg) {
			int message = msg.what;
			
			switch (message) {
			case Constant.HANDLER_VIDEOLIST_UPDATE:
				onResume();
				break;

			default:
				break;
			}
			
		};
	};
	
	
}
