package com.example.smartcaresystemvideo;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.joda.time.format.DateTimeFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcaresystemvideo.util.Constant;

public class HandleAudio extends DialogFragment {
	int mode;
	String title;
	Handler mHandler;
	Activity activity;
	MediaRecorder recorder;
	MediaPlayer player;
//	String PATH;
	String filePath;
	final static String TAG = "HandleAudio";
	final static int RECORD_MODE = 0;
	final static int PLAY_MODE = 1;
	private static HandleAudio progressDialog; 
	int currentProgress;
	ProgressBar pb;

	public static HandleAudio createInstance() {
		if (progressDialog == null) {
			progressDialog = new HandleAudio();
		}
		return progressDialog;
	}

	public static HandleAudio getInstance() {
		return progressDialog;
	}

	public void init() {
		// data 초기화
		currentProgress = 0;
		pb = null;

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		this.activity = activity;
		this.activity.getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();

		this.activity.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.progress_dialog, null);
		TextView audioTextView = (TextView) view.findViewById(R.id.audioView);
		Log.d(TAG, "mode : " + RECORD_MODE);
		switch (mode) {
		case RECORD_MODE:
			audioTextView.setText("녹음중");
			this.recordStart();
			Toast.makeText(activity, "15초간 녹음이 진행 됩니다.", Toast.LENGTH_SHORT).show();
			break;
		case PLAY_MODE:
			audioTextView.setText("재생중");
			this.playStart(filePath);
			Toast.makeText(activity, "녹음한 음성 파일이 재싱 됩니다.", Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}

		mBuilder.setView(view);

		return mBuilder.create();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		switch (mode) {
		case RECORD_MODE:
			Toast.makeText(activity, "녹음 완료", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(activity, MenuListActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(Constant.INTENT_FILEPATH, filePath);
			intent.putExtra(Constant.INTENT_ISALERT, true);
			intent.putExtra(Constant.INTENT_SELECTMODE, Constant.INTENT_SELECTAUDIO);
			activity.startActivity(intent);
			this.recordStop();
			break;
		case PLAY_MODE:
			Toast.makeText(activity, "재생 완료", Toast.LENGTH_SHORT).show();
			this.playStop();
			break;
		default:
			break;
		}

		super.onDismiss(dialog);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setHandler(Handler handler) {
		mHandler = handler;
	}

	public void setNullInstance() {
		progressDialog = null;
	}

	private void recordStart() {
		// if (recorder != null) {
		// recorder.stop();
		// recorder.release();
		// recorder = null;
		// }
		Log.d(TAG, "recordStart");
		String date = DateTimeFormat.forPattern("yyyyMMddHHmmss").print(System.currentTimeMillis());
		filePath = Constant.AUDIO_RECORD_PATH +"/"+ date +"_audio.mp4";
	
		if (recorder != null) {
			recorder.stop();
			recorder.release();
			recorder = null;
		}

		// 실험 결과 왠만하면 아래 recorder 객체의 속성을 지정하는 순서는 이대로 하는게 좋다 위치를 바꿨을때 에러가 났었음
		// 녹음 시작을 위해 MediaRecorder 객체 recorder를 생성한다.
		recorder = new MediaRecorder();

		// 오디오 입력 형식 설정
		recorder.setAudioSource(MediaRecorder.AudioSource.MIC);

		// 음향을 저장할 방식을 설정
		recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

		// 오디오 인코더 설정
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

		// 저장될 파일 지정
		recorder.setOutputFile(filePath);

		try {
			Toast.makeText(activity, "녹음이 시작되었습니다.", Toast.LENGTH_LONG).show();

			// 녹음 준비,시작
			recorder.prepare();
			recorder.start();
		} catch (Exception ex) {
			Log.e("SampleAudioRecorder", "Exception : ", ex);
		}

		Timer audioStopTimer = new Timer();
		audioStopTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				recordStop();
				dismiss();
			}
		}, 16 * 1000);

	}

	private void recordStop() {
		if (recorder != null) {
			recorder.stop();
			recorder.release();
			recorder = null;
			return;
		}
		dismiss();
	}

	private void playStart(String filePath) {
		if (player != null) {
			player.stop();
			player.release();
			player = null;
		}

		try {
			player = new MediaPlayer();
			player.setDataSource(filePath);
			player.prepare();
			player.start();
			player.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub
					Toast.makeText(activity, "녹음이 완료 되었습니다.", Toast.LENGTH_SHORT).show();
					playStop();
				}
			});
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playStop() {
		if (player == null) {
			return;
		}

		player.stop();
		player.release();
		player = null;
		dismiss();
	}
}
