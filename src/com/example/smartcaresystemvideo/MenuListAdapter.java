package com.example.smartcaresystemvideo;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
//import java.io.FileDescriptor;
//import java.io.FileInputStream;

import org.joda.time.DateTime;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcaresystemvideo.util.Constant;
import com.example.smartcaresystemvideo.util.HandleException;

public class MenuListAdapter extends BaseAdapter {
	String mode;
	Context context;
	String [] recordList;
	Handler handler;
	final static String TAG = "MenuListAdapter";
	int position;

	public MenuListAdapter(String mode, Context context,String[] recordList, Handler handler) {
		// TODO Auto-generated constructor stub
		this.mode = mode;
		this.context = context;
		this.recordList = recordList;
		this.handler = handler;
	}
	
	@Override
	public int getCount() {
		switch (mode) {
		case Constant.INTENT_SELECTVIDEO:
			
			return recordList.length;
			
		case Constant.INTENT_SELECTAUDIO:
			
			return recordList.length;
			
		case Constant.INTENT_SELECTLOG:
			
			break;

		default:
			break;
		}
		
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		String playTimeFormat = "mm:ss";
		this.position = position;
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(view == null)
			view = inflater.inflate(R.layout.selectsubmenu, parent, false);
				
		ImageView image = (ImageView) view.findViewById(R.id.image);
		TextView topTextView = (TextView) view.findViewById(R.id.TopTextView);
		TextView bottomLeftTextView = (TextView) view.findViewById(R.id.BottomLeftTextView);
		TextView bottomRightTextView = (TextView) view.findViewById(R.id.BottomRightTextView);
		Button submitButton = (Button) view.findViewById(R.id.submitButton);
		Button deleteButton = (Button) view.findViewById(R.id.deleteButton);
		
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String filePath;
				if (mode.equals(Constant.INTENT_SELECTVIDEO)) {
					filePath = Constant.VEDIO_RECORD_PATH  + "/" + recordList[position];
				} else {
					filePath = Constant.AUDIO_RECORD_PATH  + "/" + recordList[position];
				}
				
				Log.d(TAG, "mode : " + mode);
				switch(v.getId()){
				case R.id.submitButton:
					if (mode.equals(Constant.INTENT_SELECTVIDEO)) {
						new ServerSend(ServerSend.VIDEO_MODE, filePath, context);
						new ServerSend.SendThread().execute();
					} else {
						new ServerSend(ServerSend.AUDIO_MODE, filePath, context);
						new ServerSend.SendThread().execute();
					}
					
					Log.d("TAG", "click submitButton : " + position);
					break;
					
				case R.id.deleteButton:
					File file = new File(filePath);
					file.delete();
					if(handler != null)
						handler.sendMessage(Message.obtain(handler, Constant.HANDLER_VIDEOLIST_UPDATE));
					Toast.makeText(context, "삭제 되었습니다.", Toast.LENGTH_SHORT).show();
					Log.d("TAG", "click deleteButton : " + position);
					break;
				}
			}
		};
		
		submitButton.setOnClickListener(onClickListener);
		deleteButton.setOnClickListener(onClickListener);
		String filePath = null;
		DateTime time = null;
		MediaPlayer MP = null;
		int playTime;
		switch(mode) {
		case Constant.INTENT_SELECTVIDEO:
			filePath = Constant.VEDIO_RECORD_PATH  + "/" + recordList[position];
			Log.d(TAG, "VEDIO FILE PATH : " + filePath);
			Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, Thumbnails.MICRO_KIND);

			playTime = 0;
			MP = new MediaPlayer();
			
			try {
				FileInputStream fis = new FileInputStream(filePath);
				FileDescriptor fd = fis.getFD();
				MP.setDataSource(fd);
				MP.prepare();
				playTime = MP.getDuration();
				MP.release();
			} catch (Exception e) {
				HandleException.writeException(e);
			}
			time = new DateTime((long)playTime);
			
			image.setImageBitmap(thumbnail);
			topTextView.setText("파일 이름 : " + recordList[position]);
			bottomLeftTextView.setVisibility(View.GONE);
			bottomRightTextView.setText("재생 시간 : " + time.toString(playTimeFormat));
			break;
			
		case Constant.INTENT_SELECTAUDIO:
			filePath = Constant.AUDIO_RECORD_PATH  + "/" + recordList[position];
			Log.d(TAG, "AUDIO FILE PATH : " + filePath);
			playTime = 0;
			MP = new MediaPlayer();
			
			try {
				MP.setDataSource(filePath);
				MP.prepare();
				playTime = MP.getDuration();
				MP.release();
			} catch (Exception e) {
				HandleException.writeException(e);
			}
			time = new DateTime((long)playTime);
			
			image.setImageResource(R.drawable.audio);
			topTextView.setText("파일 이름 : " + recordList[position]);
			bottomLeftTextView.setVisibility(View.GONE);
			bottomRightTextView.setText("재생 시간 : " + time.toString(playTimeFormat));
			break;
			
		case Constant.INTENT_SELECTLOG:
			
			break;

		default:
			
			break;
		}
		
		
		return view;
	}
	
	

}