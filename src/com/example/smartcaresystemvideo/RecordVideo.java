package com.example.smartcaresystemvideo;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.Toast;

import com.example.smartcaresystemvideo.util.Constant;
import com.example.smartcaresystemvideo.util.HandleException;
import com.telefield.smartcaresystem.http.HttpUtils;

public class RecordVideo extends Activity implements Callback {
	private SurfaceHolder surfaceHolder;
	private SurfaceView surfaceView;
	private MediaRecorder mrec = new MediaRecorder();
	private Timer recordingTimer;
	private Camera mCamera;
	private Context context ;
	/**
	 * 30초
	 */
	private final int RECORDING_TIME = 15 * 1000;
	/**
	 * 3MB
	 */
	private final long RECORDING_SIZE = 5 * 1024 * 1024;
	private final int WIDTH = 640;
	private final int HEIGHT = 480;
//	private final String MP4 = ".mp4";
	private static String PATH = "RecordVideo";
	private static String RECORD_NAME ;
	final static String TAG = ""; 
	private static String DATE_FORMMAT = "yyyy-MM-dd hh:mm:ss";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_view);
		context = this;
		if(Camera.getNumberOfCameras() > 1) {	//카메라가 2개 이상 있는 경우(전면, 후면, 그 외)
			surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
			surfaceHolder = surfaceView.getHolder();
			surfaceHolder.addCallback(this);
			//surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			
			mCamera = Camera.open(1);
		}
		else {
			return;
		}
	}
	
	private void initRecording() throws IllegalStateException, IOException {
		if (mrec == null)
			mrec = new MediaRecorder(); // Works well
		
		mCamera.unlock();

		mrec.setCamera(mCamera);
		mrec.setPreviewDisplay(surfaceHolder.getSurface());
		mrec.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		mrec.setAudioSource(MediaRecorder.AudioSource.MIC);
		mrec.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));

		String sdcard = Environment.getExternalStorageState();
		File file = null;
		
		if (!sdcard.equals(Environment.MEDIA_MOUNTED)) {
			// SD카드가 마운트되어있지 않음
			file = Environment.getRootDirectory();
		}
		else {
			// SD카드가 마운트되어있음
			file = Environment.getExternalStorageDirectory();
		}
		
		PATH = Constant.VEDIO_RECORD_PATH;
		
		file = new File(PATH);
		if (!file.exists()) {
			// 디렉토리가 존재하지 않으면 디렉토리 생성
			file.mkdirs();
		}
		
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMMAT);
		Date date = new Date(System.currentTimeMillis());
		RECORD_NAME = "/" + format.format(date)+"_video.mp4";
		PATH = PATH + RECORD_NAME;
		
		Log.d(TAG, "filePath : " + PATH);
		Log.d(TAG, "fileExsist : " + file.exists());
		mrec.setOutputFile(PATH);
		mrec.setMaxDuration(RECORDING_TIME); // 15 seconds
		mrec.setMaxFileSize(RECORDING_SIZE);
		mrec.setVideoSize(WIDTH, HEIGHT);
		mrec.prepare();
	}
	
	protected void startRecording() throws IOException {
		mrec.start();
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		try {
			initRecording();
			startRecording();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		recordingTimer = new Timer();		
		recordingTimer.schedule(new TimerTask() {			
			@Override
			public void run() {

				finish();
			}
		}, RECORDING_TIME);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (mCamera != null) {
			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setPreviewSize(WIDTH, HEIGHT);
			mCamera.setParameters(parameters);
		}
		
		else {
			Toast.makeText(getApplicationContext(), "Camera not available!", Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mrec != null) {
			mrec.stop();
			mrec.release();
			mrec = null;
		}
		
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
//		File file = new File(Constant.RECORD_PATH + RECORD_NAME);
		
		Intent intent = new Intent(this, MenuListActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra(Constant.INTENT_FILEPATH, Constant.VEDIO_RECORD_PATH + RECORD_NAME);
		intent.putExtra(Constant.INTENT_ISALERT, true);
		intent.putExtra(Constant.INTENT_SELECTMODE, Constant.INTENT_SELECTVIDEO);
		startActivity(intent);
		
//		if(file.exists()){
//			SendVideo sendVideo = new SendVideo();
//			sendVideo.execute();
//		}
	}
	
	public static String getPath() {
		return PATH;
	}

}
