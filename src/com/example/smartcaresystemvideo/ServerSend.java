package com.example.smartcaresystemvideo;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.example.smartcaresystemvideo.util.Constant;
import com.example.smartcaresystemvideo.util.HandleException;
import com.telefield.smartcaresystem.http.HttpUtils;

public class ServerSend {
	static String TAG = "SendVideo";
	static String filePath;
	static Context context;
	
	private static int mode;
	public final static int VIDEO_MODE = 0;
	public final static int AUDIO_MODE = 1;
	public ServerSend(int mode, String filePath, Context context) {
		ServerSend.mode = mode;
		ServerSend.filePath = filePath;
		ServerSend.context = context;
	}
	static public class SendThread extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... arg0) 
		{
			try 
			{
				Log.d(TAG, "filePath : " + filePath);
				final String record_file_path = filePath;
				final File record_file = new File(record_file_path);
				switch (mode) {
				case VIDEO_MODE:
					if(record_file.isFile()) 
					{      
						String number = getMyPhoneNumber();
						final HttpEntity multipart = MultipartEntityBuilder.create()
		    					.addBinaryBody("video_file",record_file)
		    					.addTextBody("call_num",number)
		    					.build();

						HttpUtils.INSTANCE.httpUrlPOSTRequest(Constant.RECOREDING_VIDEO_UPLOAD_PATH, multipart);
			
					}   
					break;
				case AUDIO_MODE:
					if(record_file.isFile()) 
					{      
						String number = getMyPhoneNumber();
						final HttpEntity multipart = MultipartEntityBuilder.create()
		    					.addBinaryBody("audio_file",record_file)
		    					.addTextBody("call_num",number)
		    					.build();

						HttpUtils.INSTANCE.httpUrlPOSTRequest(Constant.RECOREDING_AUDIO_UPLOAD_PATH, multipart);
			
					} 
					break;	
				default:
					break;
				}
				         				
			} catch(Exception e) {
				HandleException.writeException(e);
			}
			return null;
		} 
		@Override
		protected void onPostExecute(Void result) {
			Toast.makeText(context, "파일을 서버로 전송 완료 되었습니다.", Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}
		@Override
		protected void onPreExecute() {
			Toast.makeText(context, "파일을 서버로 전송 합니다.", Toast.LENGTH_SHORT).show();
			super.onPreExecute();
		}
	}
	
	private static String getMyPhoneNumber() {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		
		String number = telephonyManager.getLine1Number();
		
		if(number != null)
		{			
			if(number.startsWith("+82"))
			{
				number = number.replace("+82", "0");
			}			
		}else{
		
		}
		
		return number;
	}
}
