package com.example.smartcaresystemvideo;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


public class LoadingActivity extends Activity{
	Activity activity;
	String TAG = "LoadingActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading);
		activity = this;
		int i = 0;
		
		while(true){
			i = (int) (Math.random() * 10 + 1);
			if(i <= 5)
				break;
		}
		
		Log.d(TAG, "progress Time = " + i);
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				startActivity(new Intent(activity, MainActivity.class));
				finish();
			}
		}, (0 * 1000) + 100);
	}
}
