package com.example.smartcaresystemvideo;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.example.smartcaresystemvideo.util.Constant;

public class MainActivity extends Activity {
	Button recordVideo, selectVideo, recordVoice, selectVoice, selectLog;
	Context context;
	HandleAudio recordAudio;
	MediaPlayer player;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        context = this;
        recordVideo = (Button)findViewById(R.id.recordVideo);
        selectVideo = (Button)findViewById(R.id.selectVideo);
        recordVoice = (Button)findViewById(R.id.recordVoice);
        selectVoice = (Button)findViewById(R.id.selectVoice);
        selectLog = (Button)findViewById(R.id.selectLog);

        recordVideo.setOnClickListener(onClickListener);
        selectVideo.setOnClickListener(onClickListener);
        recordVoice.setOnClickListener(onClickListener);
        selectVoice.setOnClickListener(onClickListener);
        selectLog.setOnClickListener(onClickListener);
        
       
    }
    
    
    
    OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			// TODO Auto-generated method stub
			Intent intent;
			switch (v.getId()) {
			case R.id.recordVideo:
				Toast.makeText(context, "15초간 동영상 촬영을 합니다.", Toast.LENGTH_LONG).show();
				Timer videoTimer = new Timer();
				videoTimer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						
						startActivity(new Intent(context, RecordVideo.class));
					}
				}, 1 * 1000);
				
			break;
			
			case R.id.selectVideo:
				intent = new Intent(context, MenuListActivity.class);		
				intent.putExtra(Constant.INTENT_SELECTMODE, Constant.INTENT_SELECTVIDEO);
				intent.putExtra(Constant.INTENT_ISALERT, false);
				startActivity(intent);
				
			break;
			
			case R.id.recordVoice:
				
				Toast.makeText(context, "15초간 음성을 녹음 합니다.", Toast.LENGTH_SHORT).show();
				Timer audioTimer = new Timer();
				audioTimer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						recordAudio = new HandleAudio();
						recordAudio.setMode(HandleAudio.RECORD_MODE);
						recordAudio.show(((Activity) context).getFragmentManager(), "Dialog");
						
					}
				}, 1 * 1000);
				break;
				
			case R.id.selectVoice:
				String filePath = Constant.AUDIO_RECORD_PATH;
				File file = new File(filePath);
				if(!file.exists())
					file.mkdirs();
				
				intent = new Intent(context, MenuListActivity.class);		
				intent.putExtra(Constant.INTENT_SELECTMODE, Constant.INTENT_SELECTAUDIO);
				intent.putExtra(Constant.INTENT_ISALERT, false);
				startActivity(intent);
			break;
				
			case R.id.selectLog:
			break;

			default:
				break;
			}
		}
	};
	
	
}
