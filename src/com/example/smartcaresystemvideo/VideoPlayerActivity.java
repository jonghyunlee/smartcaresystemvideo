package com.example.smartcaresystemvideo;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.example.smartcaresystemvideo.util.Constant;

public class VideoPlayerActivity extends Activity implements SurfaceHolder.Callback, OnPreparedListener, OnCompletionListener{
	SurfaceHolder surfaceHolder;
	MediaPlayer mPlayer;	
	String TAG = "VideoPlayerActivity";
	String PATH;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.videoplayer);
		Intent intent = getIntent();
		PATH = intent.getStringExtra(Constant.INTENT_PLAY_VIDEO_PATH);
		
		SurfaceView vedioPlayersv = (SurfaceView)findViewById(R.id.vedioPlayerSurfaceView);
		surfaceHolder = vedioPlayersv.getHolder();
		surfaceHolder.addCallback(this);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		loadVideo();
		surfaceHolder.setFixedSize(mPlayer.getVideoWidth(), mPlayer.getVideoHeight());
		Log.e(TAG, "surfaceCreated" + mPlayer.getVideoWidth() +", " + mPlayer.getVideoHeight());
//		mPlayer.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub		
	}
	
	private void loadVideo(){
		Log.e(TAG, "loadVideo PATH : " + PATH);
//		final String PATH = Constant.VEDIO_DOWNLOAD_PATH;
		try {
			if(mPlayer == null){
				mPlayer = new MediaPlayer();
				mPlayer.setDataSource(PATH);
				mPlayer.setDisplay(surfaceHolder);
				mPlayer.prepareAsync();
				mPlayer.setOnPreparedListener(this);				
				mPlayer.setOnCompletionListener(this);
//				Toast.makeText(getApplicationContext(), "영상재생을 시작 합니다.", Toast.LENGTH_SHORT).show();
//				mPlayer.start();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

	@Override
	public void onPrepared(MediaPlayer player) {
		// TODO Auto-generated method stub
		player.start();
		Log.e(TAG, "onPrepared");
		Toast.makeText(getApplicationContext(), "영상재생을 시작 합니다.", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onCompletion(MediaPlayer player) {
		Log.e(TAG, "onCompletion");
		// TODO Auto-generated method stub
		if(player != null){
			player.stop();
			player.release();
			player = null;
			mPlayer = null;
		}
		Toast.makeText(getApplicationContext(), "영상재생이 완료 되었습니다.", Toast.LENGTH_SHORT).show();
		finish();
	}
	
}
