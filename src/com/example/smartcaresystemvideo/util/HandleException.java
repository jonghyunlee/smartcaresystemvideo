package com.example.smartcaresystemvideo.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import android.util.Log;

public class HandleException {
	public static void writeException(Exception e){
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String stackTrace = sw.toString();
		Log.d("HandleException","exception ::" + stackTrace);
		e.printStackTrace();
	}
}
