package com.example.smartcaresystemvideo.util;

import android.os.Environment;

public interface Constant {
	static final String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem";
	static final String VEDIO_RECORD_PATH = ROOT_PATH + "/Recorder";
	static final String AUDIO_RECORD_PATH = ROOT_PATH + "/AudioRecorder";
	static final String FIRMWARE_PATH = ROOT_PATH + "/firmware";
	static final String IMAGE_PATH = ROOT_PATH + "/image";
	static final String VEDIO_PATH = ROOT_PATH + "/DownVideo";
	static final String VEDIO_DOWNLOAD_PATH = VEDIO_PATH + "/downLoadVideo.mp4";
	static final String RECOREDING_VIDEO_DOWNLOAD_PATH = "222.122.128.45:8080/admin/menu/video/ajax/download";
	static final String RECOREDING_VIDEO_UPLOAD_PATH = "http://222.122.128.45:8080/admin/menu/video/ajax/save";
	static final String RECOREDING_AUDIO_UPLOAD_PATH = "http://222.122.128.45:8080/admin/menu/audio/ajax/save";
	
	final static public String INTENT_PLAY_VIDEO_PATH = "PLAYVIDEOPATH";
	final static public String INTENT_PLAY_AUDIO_PATH = "PLAYVOICEPATH";
	final static public String INTENT_SELECTMODE = "SELECTMODE";
	final static public String INTENT_SELECTLOG = "SELECTLOG";
	final static public String INTENT_SELECTVIDEO = "SELECTVIDEO";
	final static public String INTENT_SELECTAUDIO = "SELECTAUDIO";
	final static public String INTENT_FILEPATH = "FILEPATH";
	final static public String INTENT_ISALERT = "ISALERT";

	final static public int HANDLER_VIDEOLIST_UPDATE = 0;
}

